from process_data import * 
from naive_bayes import * 
from evaluate_model import * 
import argparse
import os
import pandas as pd

TRAINFILE = "train_file.txt"
TESTFILE="test_file.txt"
MODELFILE="model_svm"
OUTPUTFILE="output_svm"
SPACE=" "
RESULTFILE="../results/results.txt"

def main():
    parser = argparse.ArgumentParser(description="Authorship Detection")
    parser.add_argument('--folder',default="../author/")
    parser.add_argument('--rate',default='0.6')
    args = parser.parse_args()

    folderpath = args.folder
    rate = float(args.rate)
    # vocabulary: words vocabulary where 
    #      keys are words and values are indexes 
    # trainVec : train data set, each row is a document vector
    # aVec : true assignment of authors for training data
    # testVec : test data set, each row is a document vector 
    # atestVec : true assignment of authors for training data
    # authors : author dictionary where 
    #      keys are author names, values=indexes 
    # read the folder and create datasets

    if os.path.isfile(RESULTFILE): os.remove(RESULTFILE)
    train,test,authors = read_folder(folderpath,rate)
    vocabulary = get_vocabulary(train,test)
    print "--->Create vocabulary dictionary size of ",len(vocabulary)
    (aVec,trainVec,_)= create_data_vectors(train,vocabulary,authors)
    print "--->Training data size  ",trainVec.shape
    (atestVec,testVec,testWords)= create_data_vectors(test,vocabulary,authors)
    print "--->Test data size  ",testVec.shape
    
    print "--->Apply Naive Bayes Model on training data"
    naive_bayes_model = naive_bayes(authors,vocabulary,[aVec,trainVec])
    print "--->Prediction with NB on test data"
    predicted_authors= naive_bayes_model.predict_authors([atestVec[:,0],testWords])
    print "--->Evaluation"
    evaluation = evaluate_model(atestVec[:,0], predicted_authors, authors)

    print "--->Results are saved into "+RESULTFILE
    print "--->Confusion matrix saved into ../results/conf_matrix_N.csv"
    evaluation.save_results(RESULTFILE,"Naive Bayes Model")

    print "--->Create SVM files"
    create_svm_files(trainVec,aVec,TRAINFILE)
    create_svm_files(testVec,atestVec,TESTFILE)

    print "--->Running SVM classifier"
    os.system("./svm_multiclass_learn -v 0 -c 9.0 "+TRAINFILE+SPACE+MODELFILE)
    print "--->Classifying test data with SVM "
    os.system("./svm_multiclass_classify "+TESTFILE+SPACE+MODELFILE+SPACE+OUTPUTFILE)    
    # getting svm predictions from file
    predicted_svm = np.zeros(shape=predicted_authors.shape)
    ind = 0
    with open(OUTPUTFILE,"r") as fp:
        for line in fp:
            predicted_svm[ind]=int(line.split()[0])-1    
            ind = ind+1
    evaluation = evaluate_model(atestVec[:,0], predicted_svm, authors)
    evaluation.save_results(RESULTFILE,"SVM Model")
    print "--->Results are saved into "+RESULTFILE
    print "--->Confusion matrix saved into ../results/conf_matrix_S.csv"

if __name__ == "__main__":
    main()
