import copy
import numpy as np

class naive_bayes():

    def __init__(self, author_dict, word_dict, corpusCounts):

        self.author_dict = author_dict
        self.word_dict = word_dict
        self.calculate_probabilities(corpusCounts)

    def calculate_probabilities(self,corpusCounts):
        """
        calculates probabilities for each word according to authors
        also calculates the prior probabilites for each author
        inputs:
            corpusCounts: a set of two vectors, first of which is the authors of documents
                and second of which is the token counts for each document 
        outputs:
        """
        authors = corpusCounts[0]
        documents = corpusCounts[1]
        #Calculate author prior prbabilities
        authors = authors.flatten()
        self.author_priors = np.zeros(len(self.author_dict))
        for author_id in authors:
            self.author_priors[int(author_id)] += 1
        self.author_priors = self.author_priors.astype(float)/self.author_priors.sum()

        no_authors = np.unique(authors).size
        probabilities = np.zeros([no_authors,documents.shape[1]])
        for i in np.unique(authors):
            indices = authors == i
            temp = documents[indices].sum(axis=0)
            probabilities[int(i)] = copy.deepcopy(temp)
        probabilities += 1
        probabilities = probabilities.astype(float)

        self.probabilities = probabilities/probabilities.sum(axis=1)[:,None]

    def calculate_author_probabilities(self, document):
        """
        calculates the author probabilities for a given document
        input 
            document: a list of strings that constitutes the given document
        output
            author_probabilities: a numpy array, the probability that the given document belongs to authors
        """
        author_probabilities = np.ones(len(self.author_dict))
        for word in document:
            author_probabilities = author_probabilities + np.log(self.probabilities[:,self.word_dict[word]])

        #Figuring in the author prior probabilities
        author_probabilities = author_probabilities + np.log(self.author_priors)


        return author_probabilities

    def predict_authors(self, corpus):
        """
        predicts authors for all the documents in the corpus
        input 
            corpus: a list that includes a vector of document authors and an list of lists, each list being a document
        output
            predicted authors: the id's of the predicted authors for all documents
        """
        authors = corpus[0]
        self.predicted_authors = np.zeros(len(authors)) - 1
        documents = corpus[1]
        for i in range(len(documents)):
            author_probabilities = self.calculate_author_probabilities(documents[i])
            self.predicted_authors[i] = np.argmax(author_probabilities)
        return self.predicted_authors







