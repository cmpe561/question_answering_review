# coding=utf-8
from os import listdir
from os.path import join
import io
import re
import string
from collections import Counter
from random import shuffle
import random
import numpy as np
import pickle

SPACE = " "
EMPTY=""
unknownDict= {u'o':[u'ö',u'\xd6',u'\xf6',u'Ö',u'\xc2'], \
              u's':[u'ş',u'Ş',u'\xde',u'\xfe',u'\xba',u'\xc5\x9f',u'þ'],\
              u'i':[u'ı',u'\xdd',u'\xee',u'\xfd',u'ý'],\
              u'g':[u'ð',u'\xf0',u'ð',u"\u011f"],\
              u'c':[u'\xe7',u'\xc7'],\
              u'a':[u'\xe2'],\
              u'u':[u'ü',u'ü',u'\xfb',u'\xfc',u'\xdc'],\
              SPACE:[u'\x85',u'\xbb',u'\x91',u'\x93',u'\x94',u'\x92',u'\n',u'\r',u'\t'],
              EMPTY:[u'\u2019',u'\u201c',u'\u201d',u'\u2018',u'\u2026',u'\u2013']}

punc_dict = {u'...':u' ellipsisxx ','!':u' exclamxx ',u'.':' periodxx ',
             u'?': u' qmarkxx ',u'-':u' dashxx ',u':':' colonxx ', 
             u';':u' semicolonxx ',u'*':u' asteriskxx ', u',': ' commaxx ',
             u'\"': u' dquotexx ',u'\'': u' squotexx ',
             u'(': u' lparanxx ',u')':u' rparanxx ',
}
SEED = 448 
#random.seed(SEED)
def clean_text(text):
    """ from given text, remove numbers
        and punctuations 
        Input: 
           text: string
        Output: 
           text: string, without numbers and punc.
    """
    text = text.lower()
    # remove numbers
    text = ''.join([i for i in text if not i.isdigit()])
    # replace some punctuation
    for k,v in punc_dict.iteritems():
        text = text.replace(k,v)
    # remove the remaining punctuations
    regex = re.compile('[%s]' % re.escape(string.punctuation))
    text = regex.sub("", text)
    #changing the code below

    for key in unknownDict.iterkeys():
        for v in unknownDict[key]:
            text = text.replace(v,key)
    return text

def read_text_file(authorpath,rate):
    """ read the all text files in the path 
        returns the content as array
        Input:   
           filepath : string, the file path
           rate  : the rate of training and test data 
        Output: 
          train_text_list : list of text and authorname for training
          test_text_list : list of text and authorname for test
    """  
    # get all the text files in the folder
    filelist = listdir(authorpath)
    # the number of files for training set 
    nbr_train = int(len(filelist)*rate)
    # get the author name from path name
    authorname = authorpath.split("/")[2]
    train_text_list = []
    test_text_list = []
    # shuffle the order of text files 
    shuffle(filelist)
    # for each text file 
    for f in range(len(filelist)):
        # read the content 
        with io.open(join(authorpath,filelist[f]),encoding='windows-1254') as fp:
            text = fp.read()
        # clean the text
        text = clean_text(text)
        # if training/testing
        if f<nbr_train:
            train_text_list.append([text,authorname])
        else:
            test_text_list.append([text,authorname])
    return train_text_list,test_text_list          

def read_folder(folderpath,rate):
    """ from given folder, read all 
        text files 
        Input:
          folderpath:string, where the data is stored
        Output:
          train_text_list: all text in the files for training
          test_text_list: all text in the files for testing
          authors: a dictionary, keys are author names
            values are ids 
    """
    # take all author folders in the main folder
    folders = listdir(folderpath)
    authors = []
    train_text_list = []
    test_text_list = []
    # for each author folder
    for folder in folders:
        authorpath = join(folderpath,folder)
        authors.append(folder)
        # get the training and test lists
        train_text,test_text = read_text_file(authorpath,rate) 
        train_text_list += train_text
        test_text_list += test_text
    # transform author list to a dict 
    authors = {authors[i]:i for i in range(0,len(authors))}    
    return train_text_list,test_text_list,authors

def get_vocabulary(train,test):
    """ from given text list of training and test data, 
        find unique words
        Input: 
           train,test: list of texts with the authors 
              train[0] = [text,authorname]
        Output: 
          vocabulary: dictionary, contains 
            unique words, keys are words 
                          values are indexes  
    """
    # an empty set for vocabulary
    voc_set = set()
    # for each row in the training set 
    for row in train:
        # get the unique words
        voc_set = voc_set.union(set(row[0].split()))
    # for each row in the test set
    for row in test:
        # get the unique words
        voc_set = voc_set.union(set(row[0].split()))
    # change set to a dict
    voc_set = list(voc_set)
    vocabulary = {voc_set[i]:i for i in range(0,len(voc_set))}
    return vocabulary

def get_word_count(text):
    """ return a count dictionary of the text 
        Input: 
          text: string
        Output: 
          words: dictionary, where keys are words, 
                        values are counts       
    """
    return Counter(text.split())

def create_data_vectors(text_list,vocabulary,authors):
    """ creates  vector representations of texts
         using vocabulary
        Input:
           text_list:list of text with author names 
               text_list = [text,authorname]
           vocabulary : dictionary of words
               where keys are words, values are indexes
           authors: dictionary of author names
              keys are names,values are ids
        Output:
           author_vec: a numpy array, contains author ids 
           train_vec:a numpy array, contains document vectors
           word_list: list of words of documents
     """
    # for each text 
    author_vec = np.zeros(shape=(len(text_list),1))
    train_vec = np.zeros(shape=(len(text_list),len(vocabulary)))
    word_list = []
    for ind in range(len(text_list)):
        row = text_list[ind]
        wdict= get_word_count(row[0])
        word_list.append(row[0].split())
        author_vec[ind] = authors[row[1]]
        doc_vec = np.zeros(shape=(1,len(vocabulary)))
        for k,v in wdict.iteritems():
            doc_vec[:,vocabulary[k]] = v
        train_vec[ind,:] = doc_vec
    return (author_vec,train_vec,word_list)        

def create_svm_line(xvector,yvector):
    """ create a line from vectors, 
        Input: 
          xvector: vector representation of a text
          yvector: true assignment of a text 
       Output: 
          line: string, starts with authorID 
             followed by features and weights 
    """       
    line = str(int(yvector+1))+" "
    for i in range(xvector.shape[0]):
        if xvector[i]!=0:
            line = line + str(i+1)+":1 "
        #else:
        #    line = line + str(xvector[i])+":0 "

    return line
    
def create_svm_files(textVec,yVec,filename):
    """ given vector representations,
        create svm files 
        Input: 
           textVec: numpy array of text, 
            each row represents counts of the words 
          yVec : true assignment of authors  
    """
    with open(filename,"w+") as tf:
        for ind in range(textVec.shape[0]):
            line = create_svm_line(textVec[ind,:],yVec[ind])
            tf.write(line+"\n")


