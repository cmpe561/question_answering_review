import numpy as np
import pandas as pd
import warnings

class evaluate_model():

    def __init__(self, actual_authors, predicted_authors, author_dict):
        """
        init function creates the confusion matrix and conducts all 
        evaluations automatically and stores them inside the object
        inputs
            actual_authors: an array of actual author id's
            predicted_authors: an array of predicted author id's
            author_dict: a dictionary of authors and their respective id's  
        """
        self.d = len(actual_authors)
        self.a = len(author_dict)
        self.actual_authors = actual_authors
        self.predicted_authors = predicted_authors   
        self.author_dict = author_dict
        self.construct_conf_matrix()
        self.calculate_accuracy()
        self.calculate_tpfptnfn()
        self.calculate_prf()

    def construct_conf_matrix(self):
        """
        constructs the confusion matrix and stores it into the object
        """
        self.conf_matrix = np.zeros([self.a, self.a])
        for i in range(self.d):
            self.conf_matrix[int(self.predicted_authors[i])][int(self.actual_authors[i])] += 1


    def calculate_accuracy(self):
        """
        calculates the overall accuracy and author accuracies stores it into the object
        """
        self.overall_accuracy = 0
        for i in range(len(self.author_dict)):
            self.overall_accuracy += self.conf_matrix[i][i]
        self.overall_accuracy = float(self.overall_accuracy / self.conf_matrix.sum())



    def calculate_tpfptnfn(self):
        """
        calculates the tp, fp, fn, and tn values for all classes and stores them into the object
        """
        self.tp = np.zeros(self.a)
        self.fp = np.zeros(self.a)
        self.fn = np.zeros(self.a)
        self.tn = np.zeros(self.a)

        column_sum = self.conf_matrix.sum(axis=0)
        row_sum = self.conf_matrix.sum(axis=1)
        for i in range(self.a):
            self.tp[i] = self.conf_matrix[i][i]
            self.fp[i] = row_sum[i] - self.tp[i]
            self.fn[i] = column_sum[i] - self.tp[i]
            self.tn[i] = self.conf_matrix.sum()-self.tp[i] - self.fp[i] - self.fn[i]

    def calculate_prf(self):
        """
        calculates the micro and macroaveraged p, r, and f and stores them into the object
        """
        # Calculating microaveraged p. r. f.
        self.micro_p = float(self.tp.sum()/(self.tp.sum() + self.fp.sum()))
        self.micro_r = float(self.tp.sum()/(self.tp.sum() + self.fn.sum()))
        self.micro_F =(2*self.micro_p*self.micro_r)/(self.micro_p+self.micro_r)
        # Calculating macroaveraged p. r. f.
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.macro_p_array = np.nan_to_num(np.divide(self.tp.astype(float), (self.tp + self.fp)))
            self.macro_r_array = np.nan_to_num(np.divide(self.tp.astype(float), (self.tp + self.fn)))
            self.macro_F_array = np.nan_to_num(np.divide((2*np.multiply(self.macro_p_array, self.macro_r_array)),
                                           (self.macro_p_array + self.macro_r_array)))
        self.macro_p = self.macro_p_array.mean()
        self.macro_r = self.macro_r_array.mean()
        self.macro_F = self.macro_F_array.mean()

    def save_results(self,resultfile,title):
        """ save measures to given file 
            Input: 
              resultfile: where to store the output
              title: the title of the model
            Output: -
        """
        with open(resultfile,"a+") as rf:
            rf.write("\n"+title)
            rf.write("\nOverall Accuracy : "+str(self.overall_accuracy))
            rf.write("\nMicro Averaged Measures") 
            rf.write("\n\t Precision: "+str(self.micro_p))             
            rf.write("\n\t Recall   : "+str(self.micro_r))
            rf.write("\n\t F-measure: "+str(self.micro_F))
            rf.write("\nMacro Averaged Measures") 
            rf.write("\n\t Precision: "+str(self.macro_p))             
            rf.write("\n\t Recall   : "+str(self.macro_r))
            rf.write("\n\t F-measure: "+str(self.macro_F))
        #Preparing the confusion matrix for writing to file
        author_names = [None] * len(self.author_dict)
        for author in self.author_dict:
            author_names[self.author_dict[author]] = author
        with open("../results/conf_matrix_"+title[0]+".csv", "w") as rf:
            conf_matrix = pd.DataFrame(self.conf_matrix,index=author_names,columns=author_names)
            conf_matrix.to_csv(rf)
        with open("../results/additional_results_"+title[0]+".csv", "w") as rf:
            tpfpfntn = pd.DataFrame(np.transpose(np.array([self.tp, self.fp, self.fn, self.tn])),index=author_names,columns=["TP","FP","FN","TN"])
            tpfpfntn.to_csv(rf)