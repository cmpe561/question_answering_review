import unittest
import numpy as np
import sys
sys.path.insert(0, '../src/')
from evaluate_model import *

class TestEvaluateModel(unittest.TestCase):
    def setUp(self):
        self.actual_authors =    np.array([0, 1, 2, 3, 1, 0, 3, 2, 0, 3])
        self.predicted_authors = np.array([0, 0, 2, 0, 1, 0, 0, 2, 0, 3])
        self.author_dict = {'merve': 0, 'melih': 1, 'bongiovi': 2, 'sambora': 3}
        self.model = evaluate_model(self.actual_authors, self.predicted_authors, self.author_dict)

    def test_construct_conf_matrix(self):
        self.assertTrue(np.array_equal(self.model.conf_matrix, np.array([[3,1,0,2],
                                                                         [0,1,0,0],
                                                                         [0,0,2,0],
                                                                         [0,0,0,1]])))

    def test_calculate_accuracy(self):
        self.assertEqual(self.model.accuracy, 0.7)

    def test_calculate_tpfptnfn(self):
        self.assertTrue(np.array_equal(self.model.tp, np.array([3, 1, 2, 1])))
        self.assertTrue(np.array_equal(self.model.fp, np.array([3, 0, 0, 0])))
        self.assertTrue(np.array_equal(self.model.fn, np.array([0, 1, 0, 2])))
        self.assertTrue(np.array_equal(self.model.tn, np.array([4, 8, 8, 7])))

    def test_calculate_prf(self):
        self.assertEqual(self.model.micro_p, 0.7)
        self.assertEqual(self.model.micro_r, 0.7)
        self.assertEqual(np.around(self.model.micro_F, decimals=2), 0.7)
        self.assertTrue(np.array_equal(self.model.macro_p_array, np.array([0.5, 1., 1., 1.])))
        self.assertTrue(np.array_equal(np.around(self.model.macro_r_array,decimals=2), np.array([1., 0.5, 1., .33])))
        self.assertTrue(np.array_equal(np.around(self.model.macro_F_array, decimals=2), np.array([0.67, 0.67, 1., .50])))
        self.assertEqual(np.around(self.model.macro_p, decimals=2), 0.88)
        self.assertEqual(np.around(self.model.macro_r, decimals=2), 0.71)
        self.assertEqual(np.around(self.model.macro_F, decimals=2), 0.71)

if __name__ == '__main__':
    unittest.main()

