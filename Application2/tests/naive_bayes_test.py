import unittest
import numpy as np
import sys
sys.path.insert(0, '../src/')
from naive_bayes import *

class TestNaiveBayes(unittest.TestCase):
    def setUp(self):
        self.author_dict = {'merve': 0, 'melih': 1}
        self.word_dict = {'here': 0, 'we': 1, 'go': 2}
        self.corpusCounts = [np.array([0,0,1]).astype(float),
                            np.array([
                                [4,1,0],
                                [2,0,0],
                                [1,0,6]]).astype(float)]
        self.corpus = [np.array([0,0,1]).astype(float),
                            [['here','here','here','we','here'],
                             ['here', 'here'],
                             ['go', 'here','go','go','go','go','go']]]

    def test_calculate_probabilities(self):
        model = naive_bayes(self.author_dict, self.word_dict)
        test_probs = model.calculate_probabilities(self.corpusCounts)
        self.assertTrue(np.array_equal(test_probs, np.array([[0.7, 0.2, 0.1],
                                               [0.2, 0.1, 0.7]])))

    def test_calculate_author_probabilities(self):
        model = naive_bayes(self.author_dict, self.word_dict)
        model.calculate_probabilities(self.corpusCounts)
        predicted_author_probabilities = model.calculate_author_probabilities(['here','here','here','we','here'])
        true_author_probabilities = np.array([(0.7 ** 4)*0.2, (0.2 ** 4)*0.1])
        self.assertTrue(np.array_equal(np.around(predicted_author_probabilities, decimals=5),
                                       np.around(true_author_probabilities, decimals=5)))

    def test_predict_authors(self):
        model = naive_bayes(self.author_dict, self.word_dict)
        model.calculate_probabilities(self.corpusCounts)
        predicted_authors = model.predict_authors(self.corpus)
        self.assertTrue(np.array_equal(predicted_authors, np.array([0,0,1])))

if __name__ == '__main__':
    unittest.main()

