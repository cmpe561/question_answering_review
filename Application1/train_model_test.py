#coding=utf-8

import unittest
import numpy as np

from train_model import TrainModel
from train_model import calculate_accuracy

class TestTrainModel(unittest.TestCase):
    def setUp(self):
       
        self.dataset = np.array([[['Sanal','Adj'],\
                 ['parçacıklarsa','Verb'],\
                 ['bunların','Pron'],\
                 ['hiçbirini','Pron'],\
                 ['yapamazlar','Verb'],\
                 ['.','Punc']],\
                 [['parçacıklarsa','Verb'],\
                 ['bunların','Pron'],\
                 ['hiçbirini','Pron']]])

      
        testset = np.array([[['Peşreve','Noun'],\
                 ['başlamalı','Verb'],\
                 ['bunların','Pron']],\
                 [['annemin','Noun'],\
                 ['yapamazlar','Verb'],\
                 ['.','Punc']]])

        self.model = TrainModel(self.dataset,testset)
        vocabulary = np.array(['Sanal','parçacıklasa',\
                         'bunların','hiçbirini',\
                         'yapamazlar','.'])
 
    def test_get_word_vocabulary(self):
        word_cnt = self.model.get_word_vocabulary()
        self.assertEqual(len(word_cnt),9)
        self.assertTrue('parçacıklarsa' in word_cnt)
        self.assertTrue('hiçbirini' in word_cnt)
        self.assertTrue('annemin' in word_cnt)

    def test_get_tag_vocabulary(self):
        tag_cnt = self.model.get_tag_vocabulary()
        self.assertEqual(len(tag_cnt),5)
        self.assertTrue('Pron' in tag_cnt)
        self.assertTrue('Adj' in tag_cnt)
        self.assertTrue('Noun' in tag_cnt)
 
    def test_word_tag_count(self):
        word_tag_cnt = self.model.word_tag_count()
        self.assertEqual(len(word_tag_cnt),6) 
        self.assertEqual(word_tag_cnt[('bunların','Pron')],2)
        self.assertEqual(word_tag_cnt[('Sanal','Adj')],1)
       
    def test_tag_tag_count(self):
        tag_tag_cnt = self.model.tag_tag_count()
        self.assertEqual(len(tag_tag_cnt),6)
        self.assertEqual(tag_tag_cnt[('Pron','Pron')],2)
        self.assertEqual(tag_tag_cnt[('Verb','Punc')],1)

    def test_calculate_tag_probability(self):
        tag_prob = self.model.calculate_tag_probability()
        self.assertEqual(tag_prob.shape,(5,5))
        tag_voc = self.model.get_tag_vocabulary()  
        prev_tag = tag_voc['Pron']
        next_tag = tag_voc['Pron']
        self.assertEqual(tag_prob[prev_tag][next_tag],float(3)/9)           
        next_tag = tag_voc['Verb']
        self.assertEqual(tag_prob[prev_tag][next_tag],float(2)/9)           
        prev_tag = tag_voc['Adj']
        self.assertEqual(tag_prob[prev_tag][next_tag],float(1)/3)
        next_tag = tag_voc['Punc']
        prev_tag = tag_voc['Verb']           
        self.assertEqual(tag_prob[prev_tag][next_tag],float(2)/8)


    def test_calc_prob_tag_tag(self):
        word_prob = self.model.calculate_word_probability()
        self.assertEqual(word_prob.shape,(9,5))
        tag_voc = self.model.get_tag_vocabulary()
        word_voc = self.model.get_word_vocabulary()
        tag = tag_voc['Pron']
        word = word_voc['hiçbirini'] 
        self.assertEqual(word_prob[word][tag],float(3)/13)
        word = word_voc['parçacıklarsa'] 
        tag = tag_voc['Adj']
        self.assertEqual(word_prob[word][tag],float(1)/10)
        word = word_voc['.'] 
        tag = tag_voc['Verb']
        self.assertEqual(word_prob[word][tag],float(1)/12)

    def test_calculate_accuracy(self):
        wac,sac,conf = calculate_accuracy(self.dataset,self.dataset,self.model)
        self.assertEqual(wac,1.0)
        self.assertEqual(sac,1.0)
        self.assertEqual(conf.diagonal().sum(),9)
        predicted = np.array([[['Sanal','Adj'],\
                 ['parçacıklarsa','Verb'],\
                 ['bunların','Pron'],\
                 ['hiçbirini','Adj'],\
                 ['yapamazlar','Verb'],\
                 ['.','Punc']],\
                 [['parçacıklarsa','Verb'],\
                 ['bunların','Verb'],\
                 ['hiçbirini','Punc']]])
        wac,sac,conf = calculate_accuracy(predicted,self.dataset,self.model)
        self.assertEqual(wac,float(6)/9)
        self.assertEqual(sac,0)
        tag_voc = self.model.get_tag_vocabulary()
        self.assertEqual(conf[tag_voc['Adj']][tag_voc['Adj']],1)  
        self.assertEqual(conf[tag_voc['Adj']][tag_voc['Pron']],1)  
        self.assertEqual(conf[tag_voc['Punc']][tag_voc['Pron']],1)  


        
if __name__ == '__main__':
    unittest.main()
