import argparse
import os.path
from process_data import * 
from train_model import TrainModel
from train_model import calculate_accuracy
from train_model import output_postprocessing
from generate_predictions import *
import copy
import pandas as pd
import ipdb

TRAIN_FILE = "train.pickle"
TEST_FILE = "test.pickle"
def main():
    #Arguments parsed
    parser = argparse.ArgumentParser(description="Read file and process data")
    parser.add_argument('--file',required=True,default="corpus/METUSABANCI_treebank.conll",help="File that contains dataset")
    args = parser.parse_args()
    print("---> Initializing the POS tagger")
    file_name = args.file
    #The file is read
    print("---> Reading the corpus file")
    text = read_file(file_name)
    sents = create_sentences(text)
    print("---> Segmenting sentences")
    print("---> Splitting the dataset into training and test sets")
    #Test train split is conducted
    test_train_split(sents, TRAIN_FILE, TEST_FILE)

    with open(TRAIN_FILE) as tf:
        train_set = pickle.load(tf)    
    with open(TEST_FILE) as tf:
        test_sents_actual = pickle.load(tf)
    print("---> Training the model")
    #The model training is conducted
    model = TrainModel(train_set,test_sents_actual)
    #Probabilities are obtained
    tag_vocabulary = model.tag_vocabulary
    word_vocabulary = model.word_vocabulary
    tag_probability = model.calculate_tag_probability()
    word_probability = model.calculate_word_probability()
    test_sents_pred = copy.deepcopy(test_sents_actual)
    #And are used for predicting the labels of the words in the training set
    print("---> Predicting the test set")
    pred_tags(test_sents_pred, tag_vocabulary, word_vocabulary, tag_probability, word_probability)
    print("---> Evaluating the model")
    #Calculation of the output values
    wac,sac,conf_matrix = calculate_accuracy(test_sents_pred,test_sents_actual,model)
    output_postprocessing(conf_matrix, tag_vocabulary, wac, sac)

if __name__ == "__main__":
    main()
