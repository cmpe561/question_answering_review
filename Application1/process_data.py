import zipfile
import argparse
import sys
import os.path
import numpy as np
import pickle
import random

import io
import codecs 

ZIP = ".zip"
CONLL = ".conll"
SEED = 0

def read_file(file_name):
    """ open the archive file and read its 
        content 
     Input 
        file_name :  string, contains the [archive] file path
     Output      
             text :  string, contains the text  
                  in the [achive] file 
    """
    with io.open(file_name,'r',encoding='utf8') as fp:
        text = fp.read()
    return text

def test_train_split(sents, train_file = 'train.pickle', test_file = 'test.pickle'):
    """ randomly splits sentences into training and testing data
    Input
        sents : input sentences in the form of a 3d list
    Output
        none (two pickle files)        
    """
    n = len(sents)
    split_pt = int(n * 0.90) #deciding where to split
    #random.seed(SEED)        #the randomness seed is set
    random.shuffle(sents)
    train_sents = sents[:split_pt]
    test_sents = sents[-(n-split_pt):]
    with open(train_file,'w') as file:
        pickle.dump(train_sents,file)
    with open(test_file,'w') as file:
        pickle.dump(test_sents, file)

def create_sentences(text):
    """ gets rid of the words with '-', adds the sentence
        begin and end markers, selects columns 1 and 3 in the
        sentences, creates a list of list of lists, where
        the first dimension holds the sentences, the second
        dimension words, the third dimension the word and tag.
        Then sends its output for test and train split
    Input
        text: raw text file containing the corpus
    Output
        none
    """
    words = text.split("\n")
    all_pairs = []
    all_pairs.append([u'<s>', u'<s>'])
    for i in range(len(words)):
        words[i] = words[i].split("\t")
    for i in range(len(words)):
        #Adding sentence begin and end markers added when a
        #sentence boundary is spotted
        if words[i][0] == "":
            all_pairs.append([u'</s>', u'</s>'])
            all_pairs.append([u'<s>', u'<s>'])
        else:
            temp = []
            if words[i][1] == "_":
                continue        #Omitting "_" words
            temp.append(words[i][1])
            #Taking care of the satin tag
            if words[i][3] == u'sat\u0131n' or words[i][3] == u'Sat\u0131n':
                temp.append(u'Noun')
            else:
                temp.append(words[i][3])
            all_pairs.append(temp)  #Words and their tags appended
        #import ipdb; ipdb.set_trace()
    all_pairs.append([u'</s>', u'</s>'])
    sents = []
    sent_no = -1
    #The loop below converts the list of word-pair tags into
    #a 3d list where the word-pair tags are organized into
    #sentences
    for i in range(len(all_pairs)):
        if all_pairs[i][0] == u'<s>':
            sent_no += 1
            sents.append([])
        sents[sent_no].append(all_pairs[i])
    return sents
