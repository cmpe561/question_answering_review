#coding=utf-8

import numpy as np
import collections 
import itertools
import pandas as pd

class TrainModel():
  
    def __init__(self,trainset,testset):
        # default change dataset to 2d
        self.dataset = self.transform_3d_2d(trainset)
        self.testset = self.transform_3d_2d(testset)
#        self.prob_tag_tag = np.zeros(shape)
#        self.prob_word_tag =  np.zeros()
       # self.calc_probability()
        self.tag_vocabulary = self.get_tag_vocabulary()
        self.word_vocabulary = self.get_word_vocabulary()
 
    def word_tag_count(self):
        """ returns word-tag count in the dataset
          input: dataset
             contains the tagged words
                first column = words 
             second column = tags 
          output: count_dict
             keys are (word,tag), values are counts  
        """
        tuple_dataset = [tuple(e) for e in self.dataset]
        return collections.Counter(tuple_dataset)
    
    def tag_tag_count(self):
        """ returns tag-tag count in the dataset 
          input: dataset 
           contains the tagged words 
              first column = words 
              second column = tags 
          output: count_dict
            keys are (tag,tag), values are counts 
        """     
        tags = self.dataset[:,1]
        tag_tuples = [tuple((''.join(tags[i-1]),''.join(tags[i]))) \
                           for i in range(1,len(tags))]
        return collections.Counter(tag_tuples)    

    def calculate_word_probability(self):
        """ calculates the prob of each 
          pair (word,tag) 
        input  : dataset numpy array  
        output : dict 
               keys are (word,tag)
               values are (probabilities)
       calculated with : 
           P(w_i|t_i) = count(w_i,t_i)/count(t_i) 
        """
        tags = self.tag_vocabulary
        tag_count = collections.Counter(self.dataset[:,1])
        words = self.word_vocabulary 
        pairs = []
        # get all possible pairs 
        for w in words.iterkeys():
            for t in tags.iterkeys():
                pairs.append((w,t))       
        # get the (word,tag) count in the database
        word_tags = self.word_tag_count()
        prob_word = np.zeros(shape=(len(words),len(tags)))
        # for all possible tags get the probability
        for pair in pairs:
            word = self.word_vocabulary[pair[0]]
            tag = self.tag_vocabulary[pair[1]]
            if word_tags.has_key(pair):
                pair_cnt = word_tags[pair] + 1
            else: 
                pair_cnt = 1
            # TODO: check how to calculate smoothing
            prob = float(pair_cnt)/(tag_count[pair[1]]+len(self.word_vocabulary)) 
            prob_word[word][tag]=prob 
        return prob_word     

    def calculate_tag_probability(self):
        """ calculates the prob of each 
              pair (tag,tag)
        input  : dataset numpy array  
        output : dict 
               keys are (word,tag)
               values are (probabilities)
        calculated with : 
           P(t_i|t_{i-1}) = count(t_{i-1},t_i)+1/count(t_{i-1})+|T|   
        """
        # get possible tag pairs for all tag
        tag_pairs = []
        tags = self.tag_vocabulary
        for k in tags.iterkeys():
            for j in tags.iterkeys():
                tag_pairs.append((k,j))
        # the paired tag count in the dataset
        # some paires may not be represented 
        tag_tag = self.tag_tag_count()
        tag_count = collections.Counter(self.dataset[:,1])
        prob_tag = np.zeros(shape=(len(tags),len(tags)))
        # for all possible tags get the probability
        for pair in tag_pairs:
            prev_tag = self.tag_vocabulary[pair[0]]
            next_tag = self.tag_vocabulary[pair[1]]
            if tag_tag.has_key(pair):
                pair_cnt = tag_tag[pair] + 1
            else: 
                pair_cnt = 1
            # TODO: check how to calculate smoothing
            prob = float(pair_cnt)/(tag_count[pair[0]]+len(tags)) 
            prob_tag[prev_tag][next_tag] =prob 
        return prob_tag

    def transform_3d_2d(self,dataset): 
        """ transform 3d dataset into 2d 
          input : dataset 
             each array is sentence
          output: dataset in 2d
             each sentence is combined 
         """
        data_2d = []
        for sent in dataset:
            for pair in sent:
                data_2d.append(pair)
        return np.array(data_2d)    

    def get_tag_vocabulary(self):
        allset = np.concatenate((self.testset,self.dataset),axis=0)
        tags = collections.Counter(allset[:,1])
        tdict = {tags.keys()[i]:i for i in range(0,len(tags))} 
        return tdict 

    def get_word_vocabulary(self):
        allset = np.concatenate((self.testset,self.dataset),axis=0)
        words = collections.Counter(allset[:,0])
        wdict = {words.keys()[i]:i for i in range(0,len(words))} 
        return wdict
    
        

# TODO: assuming two dataset in the same order
def calculate_accuracy(predicted_data,true_data,model):
    """ calculate accuracy using predicted and true dataset
       inputs: 
         predicted_data: an array each element is a sentence 
              a sentence is an array, each element is a tagged word
         true_data: same as predicted data
       outputs:
         word_accuracy: (number of correctly tagged words)/(total words)
         sent_accuracy: (number of correctly tagged sentence)/(total sentence)
         cont_matrix : confusion matrix for tags 
    """
    sent_correct=0;word_correct=0
    word_number=0

    # get tag pairs
    tag_pairs = []
    tags = model.tag_vocabulary
    for k in tags.iterkeys():
        for j in tags.iterkeys():
            tag_pairs.append((k,j))

    # remove <s>
    #del tags[tags.index('<s>')]
    #del tags[tags.index('</s>')]

    conf_matrix = np.zeros(shape=(len(tags),len(tags)))    
    
    for s in range(len(predicted_data)):
        # control if the sentences are the same 
        true_sent = true_data[s]
        predicted = predicted_data[s]
        sent_correct += int(predicted == true_sent)

        for w in range(len(predicted)):
            # don't need to control <s> and </s>            
            if predicted[w][1] == '<s>' or predicted[w][1] =='</s>':
                continue;
            word_correct += int(predicted[w][1] == true_sent[w][1])
            word_number+=1
            predicted_ind = tags[predicted[w][1]]
            true_ind = tags[true_sent[w][1]]
            conf_matrix[predicted_ind][true_ind] += 1    
    word_accuracy = float(word_correct)/word_number
    sent_accuracy = float(sent_correct)/len(predicted_data)
    return word_accuracy,sent_accuracy,conf_matrix  

def output_postprocessing(conf_matrix, tag_vocabulary, wac, sac):
    """ Uses accuracy data and the confusion matrix to provide improved
    output for the program, both into terminal and into a file.
       inputs: 
         conf_matrix: confusion matrix for the applcation
         tag_vocabulary: tag vocabulary including the tags and their respective indices
         wac: Word-based accuracy score
         sac: Sentence-based accuracy score
       outputs:
         In addition to printing and saving of the incoming parameters:
          F1_accuracies: The F1 accuracies of the different tags
    """
    tags = np.empty([len(tag_vocabulary)], dtype=object)
    for tag in tag_vocabulary:
        tags[tag_vocabulary[tag]] = tag
    confusion_matrix = pd.DataFrame(conf_matrix, columns=tags, index=tags)
    del confusion_matrix[u'<s>']
    del confusion_matrix[u'</s>']
    confusion_matrix = confusion_matrix.drop(u'<s>')
    confusion_matrix = confusion_matrix.drop(u'</s>')
    tags = confusion_matrix.columns
    print("---> Calculating F1 accuracies for tags")
    tag_accuracies = np.array(confusion_matrix)
    f1_accuracies = np.zeros(len(tags))
    for i in range(len(tags)):
        if tag_accuracies[i,:].sum() == 0 or tag_accuracies[:,i].sum() == 0:
            f1_accuracies[i] = np.nan
        else:
            precision = tag_accuracies[i,i] / tag_accuracies[i,:].sum()
            recall = tag_accuracies[i,i] / tag_accuracies[:,i].sum()
            f1_accuracies[i] = (2*precision*recall)/(precision + recall)
    f1_accuracies = pd.DataFrame(f1_accuracies, index = tags, columns = ['F1 Accuracies'])
    print("---> Writing to file")
    with open("output.txt",'w') as file:
        file.write("POS Tagger Test Results")
        file.write("\n\n")
        file.write("Word accuracy: ")
        file.write(repr(wac))
        file.write("\n")
        file.write("Sentence-based accuracy: ")
        file.write(repr(sac))
        file.write("\n\n")
        confusion_matrix.to_csv(file, sep='\t')
        file.write("\n")
        f1_accuracies.to_csv(file, sep='\t')
    print("---> Printing Output")
    print("\n")
    print("POS Tagger Test Results")
    print("\n")
    print("Word accuracy:"),
    print wac
    print("Sentence-based accuracy:"),
    print sac
    print "\n"
    print confusion_matrix
    print "\n"
    print f1_accuracies





     
