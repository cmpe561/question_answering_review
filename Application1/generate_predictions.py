import numpy as np
import pandas as pd

def backtrack(current_sentence, tag_vocabulary, vit_back):
    """
    input:
      current_sentence: the current sentence in the test set as a list of lists (word - tag pairs)
      tag_vocabulary:  tag vocabulary where keys are tags and indices are integers
      vit_back:  The viterbi backtracking table,
    return: 
      none (The tags of the current_sentence is altered.)
    """
    s = len(current_sentence)
    cur_tag = u'</s>'
    for i in range(s-1,-1,-1):
        current_sentence[i][1] = cur_tag
        prev_tag = vit_back[tag_vocabulary[cur_tag],i]
        cur_tag = prev_tag

def pred_tags_sent(current_sentence, tag_vocabulary, word_vocabulary, tag_probability, word_probability, returnValues = False):
    """
    Predicts tags for a given sentence. Alters the list it has been given.
    Input:
      current_sentence: the current sentence in the test set as a list of lists (word - tag pairs)
      tag_vocabulary:  tag vocabulary where keys are tags and indices are integers
      word_vocabulary: word vocabulary where keys are words and indices are integers
      tag_probability: a probability matrix where the rows are tags at time t-1 and columns are tags at time t
      word_probability: a probability matrix where the rows are words and columns are tags
    Return: 
     vit_probs: The viterbi probability table, for testing purposes
     vit_back:  The viterbi backtracking table, for testing purposes
    """
    d = len(tag_vocabulary)             #Number of tags
    s = len(current_sentence)           #Length of current sentences
    vit_probs = np.zeros([d, s])        #A matrix the size of tags x words in sentence is created for probabilities
    vit_probs[:, 0] = 1.                #The first column is all 1's
    vit_back = np.empty([d, s], dtype=object)       #A string array for backtracking
    vit_back[:, :] = ""                             #Initialized to empty characters
    vit_back[:, 1] = u'<s>'                         #Second column is initialized to sentence start token
    vit_back[:, s-1] = u'</s>'
    for i in range(1, s):                           #For all words in the sentence
        cur_word = current_sentence[i][0]
        if i == 1:                                 #If we are at the beginning
            for cur_tag in tag_vocabulary:          #For each state, calculate the probabilities
                vit_probs[tag_vocabulary[cur_tag], i] = tag_probability[
                                                            tag_vocabulary[u'<s>'], tag_vocabulary[cur_tag]] * \
                                                        word_probability[
                                                            word_vocabulary[cur_word], tag_vocabulary[cur_tag]]
            continue
        if i == s - 1:                              #If you are at the last entry
            max_prob = 0.
            argmax_prev = ""
            for prev_tag in tag_vocabulary:         #Find the most probable sentence ending
                temp_prob = tag_probability[tag_vocabulary[prev_tag], tag_vocabulary[u'</s>']] * vit_probs[
                    tag_vocabulary[prev_tag], i - 1]
                if temp_prob > max_prob:
                    argmax_prev = prev_tag
                    max_prob = temp_prob
            vit_probs[tag_vocabulary[u'</s>'], i] = max_prob        #Enter the best probability value and the
            vit_back[tag_vocabulary[u'</s>'], i] = argmax_prev      #backtracking address

            continue
        for cur_tag in tag_vocabulary:                 #All middle words in the sentence is iterated over
            max_prob = 0.
            argmax_prev = ""
            for prev_tag in tag_vocabulary:            #The best previous tag transition is selected

                temp_prob = tag_probability[tag_vocabulary[prev_tag], tag_vocabulary[cur_tag]] * word_probability[
                word_vocabulary[cur_word], tag_vocabulary[cur_tag]] * vit_probs[tag_vocabulary[prev_tag], i - 1]

                if temp_prob > max_prob:
                    argmax_prev = prev_tag
                    max_prob = temp_prob
            vit_probs[tag_vocabulary[cur_tag], i] = max_prob
            vit_back[tag_vocabulary[cur_tag], i] = argmax_prev
    backtrack(current_sentence, tag_vocabulary, vit_back)       #After the viterbi tables are ready, send the sentence for
                                                                #backtracking

    if returnValues:                    #For testing purpses
        return (vit_probs, vit_back)


def pred_tags(test_sents_pred, tag_vocabulary, word_vocabulary, tag_probability, word_probability):
    """ 
    Handles the tagging of all sentences in sequence
    Input:
      test_sents_pred: all sentences in the test set as a list of list of lists (word - tag pairs in sentences in the corpora)
      tag_vocabulary:  tag vocabulary where keys are tags and indices are integers
      word_vocabulary: word vocabulary where keys are words and indices are integers
      tag_probability: a probability matrix where the rows are tags at time t-1 and columns are tags at time t
      word_probability: a probability matrix where the rows are words and columns are tags
    Output:
        None
    """
    d = len(tag_vocabulary)
    v = len(word_vocabulary)
    n = len(test_sents_pred)
    for k in range(n):
        current_sentence = test_sents_pred[k]
        pred_tags_sent(current_sentence, tag_vocabulary, word_vocabulary, tag_probability, word_probability)




