#coding=utf-8

import unittest
import numpy as np
import io
from generate_predictions import *
import pickle
import copy


class TestGeneratePredictions(unittest.TestCase):

    def setUp(self):

        self.test_sents_pred = [[[u'<s>', u'<s>'],  [u'bana', u'Pron'],
                                 [u'uzatt\u0131', u'Verb'],[u'</s>', u'</s>']], [[u'<s>', u'<s>'],
                                [u'bana', u'Pron'], [u'</s>', u'</s>']]]
        self.tag_vocabulary = {u'<s>': 0, u'</s>': 1,  u'Pron': 2, u'Verb': 3}
        self.word_vocabulary = {u'<s>': 0,u'</s>': 1, u'bana': 2, u'uzatt\u0131':3}
        self.tag_probability = np.array([[0.0, 0.3, 0.6, 0.1],
                                         [0.0, 0.0, 0.9, 0.1],
                                         [0.1, 0.1, 0.7, 0.1],
                                         [0.2, 0.2, 0.2, 0.3]])
        self.word_probability = np.array([[1.0, 0.0, 0.1, 0.1],
                                         [0.0, 1.0, 0.1, 0.5],
                                         [0.0, 0.0, 0.7, 0.1],
                                         [0.0, 0.0, 0.1, 0.3]])

    def test_predict_sentences(self):
        test_preds = copy.deepcopy(self.test_sents_pred)
        pred_tags(test_preds, self.tag_vocabulary, self.word_vocabulary, self.tag_probability, self.word_probability)
        self.assertEqual(test_preds, [[[u'<s>', u'<s>'],
                                                [u'bana', u'Pron'],
                                                [u'uzatt\u0131', u'Pron'],
                                                [u'</s>', u'</s>']],
                                                [[u'<s>', u'<s>'],
                                                [u'bana', u'Pron'],
                                                [u'</s>', u'</s>']]])

    def test_predict_a_sentence(self):
        test_preds = copy.deepcopy(self.test_sents_pred)
        test_sentence = test_preds[0]
        pred_tags_sent(test_sentence, self.tag_vocabulary, self.word_vocabulary,
                       self.tag_probability, self.word_probability)
        self.assertEqual(test_sentence, [[u'<s>', u'<s>'],
                                       [u'bana', u'Pron'],
                                       [u'uzatt\u0131', u'Pron'],
                                       [u'</s>', u'</s>']])

    def test_viterbi_matrix(self):
        test_preds = copy.deepcopy(self.test_sents_pred)
        test_sentence = test_preds[0]
        (vit_probs, vit_back) = pred_tags_sent(test_sentence, self.tag_vocabulary, self.word_vocabulary,
                                               self.tag_probability, self.word_probability, True)
        self.assertEqual(vit_probs.all(), np.array([[1., 0., 0., 0.],
                                             [1., 0., 0., 0.00294],
                                             [1., 0.42, 0.0294, 0.],
                                             [1., 0.01, 0.0126, 0.]]).all())

        self.assertEqual(vit_back[3][3],u'</s>')
        self.assertEqual(vit_back[3][2], u'Pron')
        self.assertEqual(vit_back[0][1], u'<s>')

if __name__ == '__main__':
    unittest.main()
