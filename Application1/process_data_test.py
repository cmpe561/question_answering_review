# coding=utf-8

import unittest
import numpy as np
import io
from process_data import *
import pickle


class TestProcessData(unittest.TestCase):
    def setUp(self):
        with io.open("corpus/METUSABANCI_treebank_unittest.conll", 'r', encoding='utf8') as fp:
            self.text = fp.read()[:-1]
        self.sents = create_sentences(self.text)
        self.maxDiff = None

    def test_read_file(self):
        with io.open("corpus/METUSABANCI_treebank_unittest.conll", 'r', encoding='utf8') as fp:
            text = fp.read()
        test_text= u"﻿1	Peşreve	peşrev	Noun	Noun	A3sg|Pnon|Dat	2	OBJECT	_	_\n\
2	başlamalı	başla	Verb	Verb	Pos|Neces|A3sg	3	SENTENCE	_	_\n\
3	.	.	Punc	Punc	_	0	ROOT	_	_\n\
\n\
1	Ama	ama	Conj	Conj	_	5	S.MODIFIER	_	_\n\
2	annemin	anne	Noun	Noun	A3sg|P1sg|Gen	3	POSSESSOR	_	_\n\
3	şartları	şart	Noun	Noun	A3pl|P3sg|Nom	4	SUBJECT	_	_\n"
        #import pdb; pdb.set_trace()
        self.assertEqual(text, test_text)
    def test_process_data(self):
        expectedList = [[['<s>','<s>'],[u'Peşreve',u'Noun'],[u'başlamalı',u'Verb'],[u'.',u'Punc'],['</s>','</s>']],[['<s>','<s>'],[u'Ama',u'Conj'],[u'annemin',u'Noun'],[u'şartları',u'Noun'],['</s>','</s>']]]
        self.sents = create_sentences(self.text)
        self.assertEqual(create_sentences(self.text), expectedList)

    def test_save_load_pickle(self):
        test_train_split(self.sents, 'train_unittest.pickle', 'test_unittest.pickle')
        with open('train_unittest.pickle','r') as file:
            train = pickle.load(file)
        with open('test_unittest.pickle','r') as file:
            test = pickle.load(file)
        self.assertEqual(len(train), 1)
        self.assertEqual(len(test), 1)

if __name__ == '__main__':
    unittest.main()
